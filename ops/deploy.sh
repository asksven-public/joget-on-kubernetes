#!/bin/bash

[[ -z "${CI_ENVIRONMENT_NAME}" ]] && { echo "CI_ENVIRONMENT_NAME is empty" ; exit 1; }
[[ ! -f "./variables/${CI_ENVIRONMENT_NAME}.yaml" ]] && { echo "file ./variables/${CI_ENVIRONMENT_NAME}.yaml does not exist" ; exit 1; }
[[ -z "$NAMESPACE" ]] && { echo "Error: NAMESPACE can not be empty"; exit 1; }
[[ -z "$CI_COMMIT_REF_SLUG" ]] && { echo "Error: CI_COMMIT_REF_SLUG can not be empty"; exit 1; }


#!/bin/bash

echo "Branch: $CI_BUILD_REF_SLUG"

echo "Deploying into $NAMESPACE" 

K8S_DIR=./manifests
TARGET_DIR=${K8S_DIR}/.generated
mkdir -p ${TARGET_DIR}
mkdir -p ${TARGET_DIR}/variables

echo "Expanding config files"
jinja2 $f ./variables/${CI_ENVIRONMENT_NAME}.yaml \
--format=yaml --strict \
-D CI_COMMIT_REF_SLUG=${CI_COMMIT_REF_SLUG} \
> "${TARGET_DIR}/variables/${CI_ENVIRONMENT_NAME}.yaml"

echo "Expanding yaml files"
for f in ${K8S_DIR}/*.yaml
do
  jinja2 $f ${TARGET_DIR}/variables/${CI_ENVIRONMENT_NAME}.yaml \
  --format=yaml --strict \
  -D NAMESPACE=${NAMESPACE} \
  > "${TARGET_DIR}/$(basename ${f})"
done

echo "Creating/updating namespace"
# Apply namespace first, since it is a pre-req for the rest
kubectl apply -f ${TARGET_DIR}/namespace.yaml

# Label temp and testing namespaces with TTL 1d
if [ "$CI_ENVIRONMENT_NAME" = "fb" ]
then
    kubectl annotate namespace $NAMESPACE janitor/ttl=1d
fi


kubectl -n $NAMESPACE create secret generic mysql-credentials --from-literal=MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}
kubectl -n $NAMESPACE create secret generic minio-credentials --from-literal=S3_KEY=${STORAGE_ACCOUNT_NAME} --from-literal=S3_SECRET="${STORAGE_ACCOUNT_KEY}"

echo "Deploying"
kubectl --namespace=$NAMESPACE apply -f ${TARGET_DIR}
