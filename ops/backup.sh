#!/bin/bash

if [ -z "$1" ]; then
echo "Syntax is: backup.sh <job-name>"
exit 1
fi

echo "Branch: $CI_BUILD_REF_SLUG"

echo "Applying into $NAMESPACE" 



K8S_DIR=./jobs
TARGET_DIR=${K8S_DIR}/.generated
mkdir -p ${TARGET_DIR}


echo "Expanding yaml files"
for f in ${K8S_DIR}/*.yaml
do
  jinja2 $f ./variables/${CI_ENVIRONMENT_NAME}.yaml \
  --format=yaml --strict \
  -D JOB_NAME=${1} \
  > "${TARGET_DIR}/$(basename ${f})"
done

echo "Starting backup job"
kubectl --namespace=$NAMESPACE apply -f ${TARGET_DIR}/backup-job.yaml
