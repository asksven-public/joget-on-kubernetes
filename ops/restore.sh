#!/bin/bash

if [ -z "$1" ]; then
echo "Syntax is: restore.sh <job-name>"
exit 1
fi

echo "Branch: $CI_BUILD_REF_SLUG"

echo "Applying into $NAMESPACE" 



K8S_DIR=./jobs
TARGET_DIR=${K8S_DIR}/.generated
mkdir -p ${TARGET_DIR}


echo "Expanding yaml files"
for f in ${K8S_DIR}/*.yaml
do
  jinja2 $f ./variables/${DEPLOY_ENV}.yaml \
  --format=yaml --strict \
  -D JOB_NAME=${1} \
  -D NAMESPACE=${NAMESPACE} \
  -D DEPLOY_ENV=${DEPLOY_ENV} \
  > "${TARGET_DIR}/$(basename ${f})"
done

# Scale down Jira since it holds a database connect
kubectl -n $NAMESPACE scale deploy jira --replicas=0
kubectl -n $NAMESPACE wait --timeout=600s --for=delete pod -l app=jira

# Scale down postgres-exporter since it holds a database connect
kubectl -n $NAMESPACE scale deploy postgres-exporter --replicas=0
kubectl -n $NAMESPACE wait --timeout=600s --for=delete pod -l app=postgres-exporter

echo "Deleting previous jobs with the same name"
JOBNAME=${1/_/-}

kubectl -n $NAMESPACE delete job jira-restore-${JOBNAME}


echo "Starting backup job"
kubectl --namespace=$NAMESPACE apply -f ${TARGET_DIR}/restore-job.yaml
kubectl -n $NAMESPACE wait --timeout=1200s --for=condition=complete job -l app=jira-attended-restore
echo "Restore is completed. Scaling back up"

# Scale everything back up
kubectl -n $NAMESPACE scale deploy jira --replicas=1
kubectl -n $NAMESPACE scale deploy postgres-exporter --replicas=1
