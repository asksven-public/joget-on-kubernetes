# Install joget

https://dev.joget.org/community/display/DX7/Joget+on+Kubernetes#JogetonKubernetes-DeployMySQLonKubernetes

## Configure

Create `ops/setenv` and define the environment variables:

```
export CI_ENVIRONMENT_NAME=<name-of-the-variables-files>
echo "Setting CI_ENVIRONMENT_NAME to $CI_ENVIRONMENT_NAME"

export NAMESPACE=<target-namespace>
echo "Setting NAMESPACE to $NAMESPACE"

export CI_COMMIT_REF_SLUG=main
echo "Setting NAMESPACE to $CI_COMMIT_REF_SLUG"

export STORAGE_ACCOUNT_KEY="the storage account key backups will be sent to"
export STORAGE_ACCOUNT_NAME=<the-name-of-the-storage-account>
export MYSQL_ROOT_PASSWORD=<mysql-root-password>
```
## Deploy
```
cd ops
source setenv
./deploy
```

## Update ingress-nginx

For joget to run properly we need to update ingress-ngnix config to allow the support of headers with underscores:

1. edit: `kubectl -n ingress edit cm ingress-internal-asksven-io-ingress-nginx-controller`
1. Add to data: 

```
data:
  enable-underscores-in-headers: "true"
```  
3. Restart ingress-ngnix